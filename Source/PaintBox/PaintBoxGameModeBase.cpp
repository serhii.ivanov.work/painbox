// Copyright Epic Games, Inc. All Rights Reserved.


#include "PaintBoxGameModeBase.h"
#include "PlayerShape.h"
#include "PBHUD.h"


APaintBoxGameModeBase::APaintBoxGameModeBase()
{
	DefaultPawnClass = APlayerShape::StaticClass();
	HUDClass = APBHUD::StaticClass();
}
