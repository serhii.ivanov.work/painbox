// Copyright Epic Games, Inc. All Rights Reserved.

#include "PaintBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PaintBox, "PaintBox" );
