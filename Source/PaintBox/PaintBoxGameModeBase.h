// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PaintBoxGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PAINTBOX_API APaintBoxGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	APaintBoxGameModeBase();
};
