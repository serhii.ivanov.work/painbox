// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TargetBox.generated.h"

class UStaticMeshComponent;
class UMaterialInstanceConstant;
class UBoxComponent;
class USoundBase;

UENUM(BlueprintType, Category = "SpawnBox")
enum class SpawnBox :uint8
{
	Clean = 0 UMETA(DisplayName = "Clin"),
	Cleaner = 1 UMETA(DisplayName = "Cliner"),
	Marker = 2 UMETA(DisplayName = "Marker")
};

UCLASS()
class PAINTBOX_API ATargetBox : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATargetBox();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* VisualMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	SpawnBox BoxType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Impulse;
	UPROPERTY()
	USoundBase* Sound;

private:
	UPROPERTY()
	UStaticMeshComponent* MeshComponent;
	UPROPERTY()
	UMaterialInstanceDynamic* BoxMaterialInstance;
	UPROPERTY()
	FTimerHandle RotateTimer;
	UPROPERTY()
	FTimerHandle MoveTimer;
	UPROPERTY()
	UBoxComponent* CollisionBox;

public:
	UFUNCTION()
	void SetBoxType(int32 InType);
private:
	UFUNCTION()
	void SetMaterial();
	UFUNCTION()
	void ShapeMove();
	UFUNCTION()
	void ShapeRotation();
	UFUNCTION()
	void MarkerShape(AActor* OverlappedActor, AActor* OtherActor);
};
