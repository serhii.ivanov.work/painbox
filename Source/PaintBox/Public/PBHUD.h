// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PBHUD.generated.h"


class UWidgetComponent;
class UUserWidget;

UCLASS()
class PAINTBOX_API APBHUD : public AHUD
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	APBHUD();

private:
	UPROPERTY()
	UWidgetComponent* WidgetComp;
	UPROPERTY()
	UUserWidget* EndGameWidget;
};
