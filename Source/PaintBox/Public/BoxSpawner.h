// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BoxSpawner.generated.h"

UCLASS()
class PAINTBOX_API ABoxSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoxSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsGameOver = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MarkerBox = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CleanBox = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CleanerBox = 0;

private:
	UPROPERTY()
	int32 BoxCount;
	UPROPERTY()
	FTimerHandle CheckTimer;
	UPROPERTY()
	TArray<AActor*> FoundBoxes;

private:
	UFUNCTION()
	void SpawnBoxes();
	UFUNCTION()
	void CheckBox();
	UFUNCTION()
	void ShowBoxCounts();
	UFUNCTION()
	void EndGame(bool InEnd);
};
