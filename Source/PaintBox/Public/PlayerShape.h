// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerShape.generated.h"

class UStaticMeshComponent;
class UMaterialInstanceConstant;
class USpringArmComponent;
class UCameraComponent;
class USphereComponent;

UCLASS()
class PAINTBOX_API APlayerShape : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerShape();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerParameters")
	UStaticMesh* VisualMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerParameters")
	UStaticMesh* PointVisualMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Speed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerParameters")
	USphereComponent* CollisionSphere;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerParameters")
	float Impulse;

private:
	UPROPERTY()
	UStaticMeshComponent* MeshComponent;
	UPROPERTY()
	UStaticMeshComponent* PointMeshComponent;
	UPROPERTY()
	UMaterialInstanceDynamic* ShapeMaterialInstance;
	UPROPERTY()
	USpringArmComponent* SpringArm;
	UPROPERTY()
	UCameraComponent* PlayerCamera;

private:
	UFUNCTION()
	void SetMaterial();
	UFUNCTION()
	void ShapeMove(float value);
	UFUNCTION()
	void MouseRotate(float value);
	UFUNCTION()
	void StabilizationShape();
};
