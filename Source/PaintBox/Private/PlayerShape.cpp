// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerShape.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SphereComponent.h"
#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "BoxSpawner.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerShape::APlayerShape()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlayerShape"));
	MeshComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh>PlaerSphere(TEXT("StaticMesh'/Game/PaintBox/Shapes/Shape_Sphere.Shape_Sphere'")); 
	if (PlaerSphere.Succeeded())
	{
		VisualMesh = PlaerSphere.Object;
	}
	MeshComponent->SetAngularDamping(10.f);
	MeshComponent->SetStaticMesh(VisualMesh);
	MeshComponent->SetGenerateOverlapEvents(true);
	MeshComponent->SetSimulatePhysics(true);

	CollisionSphere = CreateAbstractDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetupAttachment(MeshComponent);
	CollisionSphere->SetWorldLocation(FVector(0.f, 0.f, 50.f));
	CollisionSphere->SetWorldScale3D(FVector(1.8f, 1.8f, 1.8f));
	CollisionSphere->SetCollisionProfileName("BlockAll");
	
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	SpringArm->SetupAttachment(MeshComponent);
	SpringArm->SetWorldLocation(FVector(0.f, 0.f, 50.f));
	SpringArm->SetWorldRotation(FRotator(330, 0.f, 0.f));
	SpringArm->TargetArmLength = 400.f;

	PointMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PointDirection"));
	PointMeshComponent->SetupAttachment(MeshComponent);
	PointMeshComponent->SetWorldLocation(FVector(-50.f, 0.f, 100.f)); 
	PointMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	ConstructorHelpers::FObjectFinder<UStaticMesh>Point(TEXT("StaticMesh'/Game/PaintBox/Shapes/Point.Point'"));
	if (Point.Succeeded())
	{
		PointVisualMesh = Point.Object;
	}
	PointMeshComponent->SetStaticMesh(PointVisualMesh);

	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	PlayerCamera->SetupAttachment(SpringArm);
}

// Called when the game starts or when spawned
void APlayerShape::BeginPlay()
{
	Super::BeginPlay();
	SetMaterial();
	ShapeMaterialInstance->SetScalarParameterValue(TEXT("RedParam"), 1.f);
}

// Called every frame
void APlayerShape::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	StabilizationShape();
}

// Called to bind functionality to input
void APlayerShape::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("ForwardBack", this, &APlayerShape::ShapeMove);
	PlayerInputComponent->BindAxis("MouseAxisX", this, &APlayerShape::MouseRotate);
}

void APlayerShape::SetMaterial()
{
	FString MaterialPath = "/Game/PaintBox/Materials/MaterialForSapes_Inst.MaterialForSapes_Inst";
	UMaterialInterface* MaterialInterface = LoadObject<UMaterialInterface>(NULL, *MaterialPath);
	if (MaterialInterface)
	{
		ShapeMaterialInstance = UMaterialInstanceDynamic::Create(MaterialInterface, MeshComponent);
		if (ShapeMaterialInstance)
		{
			MeshComponent->SetMaterial(0, ShapeMaterialInstance);
		}
	}
}

void APlayerShape::ShapeMove(float value)
{
	if (value > 0)
	{
		Impulse = 10000;
		FVector MoveDirection = MeshComponent->GetForwardVector() * Impulse;
		MeshComponent->AddImpulse(MoveDirection);
	}
	FVector Velocity=MeshComponent->GetComponentVelocity();
	Speed = Velocity.Size()/3.6f;
	PointMeshComponent->SetRelativeScale3D(FVector(1.f + Speed / 10.f, 1.f, 1.f));
}

void APlayerShape::MouseRotate(float value)
{
	if (value != 0)
	{
		RootComponent->AddLocalRotation(FRotator(0.f, value, 0.f));
	}

}

void APlayerShape::StabilizationShape()
{
	FRotator RelativeRotation = RootComponent->GetComponentRotation();
	RootComponent->SetWorldRotation(FRotator(0.f, RelativeRotation.Yaw, 0.f));
}