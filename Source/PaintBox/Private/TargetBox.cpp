// Fill out your copyright notice in the Description page of Project Settings.


#include "TargetBox.h"
#include "PlayerShape.h"
#include "Components/BoxComponent.h"
#include "Sound/SoundBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATargetBox::ATargetBox()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BoxShape"));
	MeshComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh>TargetBox(TEXT("StaticMesh'/Game/PaintBox/Shapes/Shape_Cube.Shape_Cube'"));
	if (TargetBox.Succeeded())
	{
		VisualMesh = TargetBox.Object;
	}
	MeshComponent->SetAngularDamping(5.f);
	MeshComponent->SetStaticMesh(VisualMesh);
	MeshComponent->SetGenerateOverlapEvents(true);
	MeshComponent->SetSimulatePhysics(true);
	MeshComponent->SetWorldScale3D(FVector(0.5f, 0.5f, 0.5f));

	CollisionBox = CreateAbstractDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->SetupAttachment(MeshComponent);
	CollisionBox->SetWorldLocation(FVector(0.f, 0.f, 50.f));
	CollisionBox->SetWorldScale3D(FVector(2.f, 2.f, 2.f));

	ConstructorHelpers::FObjectFinder<USoundBase>Asset(TEXT("SoundWave'/Game/PaintBox/HitSound.HitSound'"));
	if (Asset.Succeeded())
	{
		Sound = Asset.Object;
	}

	SetMaterial();
	SetBoxType(rand() % 2);
}

// Called when the game starts or when spawned
void ATargetBox::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(RotateTimer, this, &ATargetBox::ShapeRotation, 0.5f, true, 0.5f);
	GetWorld()->GetTimerManager().SetTimer(MoveTimer, this, &ATargetBox::ShapeMove, 1.0f, true, 1.0f);
	OnActorBeginOverlap.AddDynamic(this, &ATargetBox::MarkerShape);
}

// Called every frame
void ATargetBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ATargetBox::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ATargetBox::SetMaterial()
{
	FString MaterialPath = "/Game/PaintBox/Materials/MaterialForSapes_Inst.MaterialForSapes_Inst";
	UMaterialInterface* MaterialInterface = LoadObject<UMaterialInterface>(NULL, *MaterialPath);
	if (MaterialInterface)
	{
		BoxMaterialInstance = UMaterialInstanceDynamic::Create(MaterialInterface, MeshComponent);
		if (BoxMaterialInstance)
		{
			MeshComponent->SetMaterial(0, BoxMaterialInstance);
		}
	}
}

void ATargetBox::ShapeMove()
{
	Impulse = 30000;
	FVector MoveDirection = MeshComponent->GetForwardVector() * Impulse*FVector(1.f,1.f,0.f);
	MeshComponent->AddImpulse(MoveDirection);
}

void ATargetBox::ShapeRotation()
{
	RootComponent->AddLocalRotation(FRotator(0.f, (rand() % 91) - 45, 0.f));
}

void ATargetBox::MarkerShape(AActor* OverlappedActor, AActor* OtherActor)
{
	ATargetBox* Box = Cast<ATargetBox>(OtherActor);
	if (Box)
	{
		if (Box->BoxType == SpawnBox::Marker && this->BoxType==SpawnBox::Clean)
		{
			SetBoxType(2);
		}
		if (Box->BoxType == SpawnBox::Cleaner && this->BoxType==SpawnBox::Marker)
		{
			SetBoxType(0);
		}
	}
	APlayerShape* PlayerShape = Cast<APlayerShape>(OtherActor);
	if (PlayerShape && this->BoxType != SpawnBox::Marker)
	{
		UGameplayStatics::PlaySoundAtLocation(this, Sound, GetActorLocation());
		SetBoxType(2);
	}
}

void ATargetBox::SetBoxType(int32 InType)
{
	switch (InType)
	{
	case 0:
	{
		BoxType = SpawnBox::Clean;
		BoxMaterialInstance->SetScalarParameterValue(TEXT("RedParam"), 1.f);
		BoxMaterialInstance->SetScalarParameterValue(TEXT("GreenParam"), 1.f);
		BoxMaterialInstance->SetScalarParameterValue(TEXT("EmissiveStrange"), 1.f);
		break;
	}
	case 1:
	{
		BoxType = SpawnBox::Cleaner;
		BoxMaterialInstance->SetScalarParameterValue(TEXT("RedParam"), 0.f);
		BoxMaterialInstance->SetScalarParameterValue(TEXT("GreenParam"), 1.f);
		BoxMaterialInstance->SetScalarParameterValue(TEXT("EmissiveStrange"), 1.f);
		break;
	}
	case 2:
	{
		BoxType = SpawnBox::Marker;
		BoxMaterialInstance->SetScalarParameterValue(TEXT("RedParam"), 1.f);
		BoxMaterialInstance->SetScalarParameterValue(TEXT("GreenParam"), 0.f);
		BoxMaterialInstance->SetScalarParameterValue(TEXT("EmissiveStrange"), 0.f);
		break;
	}
	default:
		break;
	}
}