// Fill out your copyright notice in the Description page of Project Settings.


#include "BoxSpawner.h"
#include "TargetBox.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABoxSpawner::ABoxSpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABoxSpawner::BeginPlay()
{
	Super::BeginPlay();
	SpawnBoxes();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetBox::StaticClass(), FoundBoxes);
	GetWorld()->GetTimerManager().SetTimer(CheckTimer, this, &ABoxSpawner::CheckBox, 0.5f, true, 0.5f);
}

// Called every frame
void ABoxSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABoxSpawner::CheckBox()
{
	bool AllRed = false;
	for (int32 i = 0; i < FoundBoxes.Num(); i++)
	{
		ATargetBox* Box = Cast<ATargetBox>(FoundBoxes[i]);
		if (Box->BoxType == SpawnBox::Marker) AllRed = true;
		else
		{
			AllRed = false;
			break;
		}
	}
	ShowBoxCounts();
	if (AllRed == true)
	{
		EndGame(AllRed);
	}
}

void ABoxSpawner::ShowBoxCounts()
{
	int32 CleanBoxCount = 0;
	int32 CleanerBoxCount = 0;
	int32 MarkerBoxCount = 0;

	for (int32 i = 0; i < FoundBoxes.Num(); i++)
	{
		ATargetBox* Box = Cast<ATargetBox>(FoundBoxes[i]);
		if (Box->BoxType==SpawnBox::Clean) CleanBoxCount++;
		else if(Box->BoxType == SpawnBox::Cleaner) CleanerBoxCount++;
		else if(Box->BoxType == SpawnBox::Marker) MarkerBoxCount++;
	}
	CleanBox=CleanBoxCount;
	CleanerBox=CleanerBoxCount;
	MarkerBox=MarkerBoxCount;
}

void ABoxSpawner::SpawnBoxes()
{
	BoxCount = rand() % 10 + 5;
	for (int32 i = 0; i < BoxCount; i++)
	{
		ATargetBox* NewBox;
		NewBox = GetWorld()->SpawnActor<ATargetBox>(FVector(rand() % 1001-500, rand() % 1001-500, rand() % 2000), FRotator(0, 0, 0));
	}
}

void ABoxSpawner::EndGame(bool InEnd)
{	
	IsGameOver = InEnd;
	UGameplayStatics::SetGamePaused(GetWorld(), true);
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	PlayerController->SetShowMouseCursor(true);
}

