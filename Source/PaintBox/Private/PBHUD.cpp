// Fill out your copyright notice in the Description page of Project Settings.


#include "PBHUD.h"

#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"

void APBHUD::BeginPlay()
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (PlayerController)
	{
		if (EndGameWidget)
		{
			EndGameWidget->AddToViewport();
		}
	}
}

APBHUD::APBHUD()
{
	WidgetComp = CreateAbstractDefaultSubobject<UWidgetComponent>(TEXT("EndGameWidget"));
	EndGameWidget = CreateDefaultSubobject<UUserWidget>(TEXT("UserWidget"));

	static ConstructorHelpers::FClassFinder<UUserWidget>TextWidget(TEXT("/Game/PaintBox/MainWidget"));
	if (!ensure(TextWidget.Class != nullptr))return;
	EndGameWidget = CreateWidget<UUserWidget>(GetWorld(), TextWidget.Class);
}
